import collections
import configparser
import enum
import functools
import inspect
import itertools
import math
import pathlib
import pickle
import re
import statistics
import typing

import numpy
import pandas

try:
    import importlib.resources as pkg_resources
except ImportError:
    import importlib_resources as pkg_resources


class SequenceType(enum.Enum):
    """
    Allele Sequence Type Handler
    """
    ALIGNED = 0
    ALLELE = 1


class ImmunogenicityUtils:
    """
    Immunogenicity utilities to handle calculating mismatch scores between
    individuals.

    """

    def __init__(self):
        self._allele_conversions = None
        self._allele_mappings = None
        self._aligned_sequences = None
        self._allele_sequences = None
        self._config = None
        self._amino_acids = None
        self._haplotype_frequencies = None

    @property
    def config(self) -> typing.Dict:
        """
        Returns a data dictionary of configuration parameters.

        Returns
        -------
        dictionary
            A dictionary of configuration parameters and their associated
            values.
        """

    @config.setter
    def config(
        self, value: typing.Optional[typing.Union[str, pathlib.Path]]
    ) -> configparser.ConfigParser():
        """
        Set the file path source for configuration file before calling
        :meth:`_make_config` to build the configuration parameters.

        Parameters
        ----------
        value
            str or Path-like representation of INI file containing configuration
            parameters.
        """

    def _make_config(self, value=None):
        """
        Construct a dictionary of configuration values from an INI file.

        Parameters
        ----------
        value
            str or Path-like representation an INI file containing configuration
            parameters.
        """

    def amino_acid_details(
        self, allele: str, dict_type: SequenceType = SequenceType.ALLELE
    ) -> typing.Optional[typing.Dict]:
        """
        Returns a sequence of 276 Amino Acids(AA) for the given allele where each
        AA replaced with a pair of HB and PI

        Returns a list of lists similar to the following : [

          [0.0, 5.97], [0.3, 5.68], [-0.5, 7.47], [0.3, 5.68], [-1.3, 5.74],

          [3.0, 11.15], [-2.3, 5.66], [-2.5, 5.48], [-2.5, 5.48], [-0.4, 5.64],

          [0.3, 5.68], [-1.5, 5.96], [0.3, 5.68], [3.0, 11.15], [0.0, 6.3],

          ...

          [-1.8, 5.98], [0.0, 6.3], [3.0, 9.59], [0.0, 6.3], [-1.8, 5.98],

          [-0.4, 5.64], [-1.8, 5.98], [3.0, 11.15], [-3.4, 5.89], [3.0, 3.22],

          [-1.8, 5.98]

        ]

        where each list element is a list of HB, and PI

        Parameters
        ----------
        allele
            str of target allele
        dict_type
            str of target dictionary, currently `allele` for allele
            sequences or `aligned` for aligned sequences

        Returns
        -------
        dictionary
            A dictionary of allele values with mapped amino acids and their respective
            hydrophobicity values and isoelectric points.

        """

    @property
    def allele_sequences(
        self,
    ) -> typing.DefaultDict[str, typing.List[typing.List[float]]]:
        """
        Returns a data lookup dictionary for allele sequences.

        Produces a dictionary similar to the following: {

          'DQB1*0201' : [[3.0, 11.15], [3.0, 2.77], ...],

          'DQB1*0202' : [[3.0, 11.15], [3.0, 2.77], ...],

          ...

          'DPA1*0303' : [[-0.5, 6.0], [0.0, 5.97], ...],

          'DPA1*0401' : [[-0.5, 6.0], [0.0, 5.97], ...],

        }

        where keys are alleles and the list is an ordered sequence of their
        amino acid hydrophobicity value and isoelectric point respectively.

        Returns
        -------
        dictionary
            A dictionary of alleles and their associated sequence of amino acid
            HB and PI values.
        """

    @allele_sequences.setter
    def allele_sequences(self, value: typing.Union[str, pathlib.Path]) -> None:
        """
        Set the directory path source for allele sequence definitions before
        calling :meth:`_make_allele_sequences` to build the dictionary
        representation. Additionally, a new pickle file is generating using the
        updated file definitions to reduce future computation time.

        Parameters
        ----------
        value
            str or Path-like representation of directory containing CSV files
            with definitions for allele sequences. Each file is assumed to be of
            the form:

                DPA1*0103,DPA1*0104,...,DPA1*0303,DPA1*0401

            where each column is an allele and the following rows represent the
            amino acid sequence of the respective allele.

        """

    def _make_allele_sequences(
        self,
        ref_data: typing.Union[str, pathlib.Path] = None,
    ) -> None:
        """
        Construct a dictionary of allele values with ordered sequence of amino
        acid hydrophobicity values and isoelectric points.

        Parameters
        ----------
        ref_data
            str or Path-like representation of directory of CSV file/s or a CSV
            file containing definitions for amino acids.
        """

    @property
    def aligned_sequences(self) -> typing.Dict:
        """
        Returns a data lookup dictionary for aligned allele sequences.

        Produces a dictionary similar to the following: {

          ['A*01:01': [[0.0, 5.97], [0.3, 5.68], ...],

          'A*01:02' : [[0.0, 5.97], [0.3, 5.68], ...],

          ...

          'A*68:36': [[0.0, 5.97], [0.3, 5.68], ...],

          'A*74:11': [[0.0, 5.97], [0.3, 5.68], ...]],

        }

        where keys are alleles and the list is an ordered sequence of their
        amino acid hydrophobicity value and isoelectric point respectively.

        Returns
        -------
        dictionary
            A dictionary of aligned alleles and their associated sequence of
            amino acid HB and PI values.

        """

    @aligned_sequences.setter
    def aligned_sequences(self, value: typing.Union[str, pathlib.Path]) -> None:
        """
        Set the directory path source for aligned allele sequence definitions before
        calling :meth:`_make_aligned_sequences` to build the dictionary
        representation. Additionally, a new pickle file is generating using the
        updated file definitions to reduce future computation time.

        Parameters
        ----------
        value
            str or Path-like representation of directory containing CSV files
            with definitions for allele sequences. Each file is assumed to be of
            the form:

                A*01:01,A*01:02,A*01:03,...,A*02:02,A*02:03

            where each column is an aligned allele and the following rows represent the
            amino acid sequence of the respective aligned allele.

        """

    def _make_aligned_sequences(
        self,
        ref_data: typing.Union[str, pathlib.Path] = None,
    ) -> None:
        """
        Construct a dictionary of aligned allele values with ordered sequence
        of amino acid hydrophobicity values and isoelectric points.

        Parameters
        ----------
        ref_data
            str or Path-like representation of CSV file.
        """

    @property
    def amino_acids(self) -> typing.Dict:
        """
        Returns a data lookup dictionary for amino acids.

        Returns a dictionary similar to the following: {

          'A': [-0.5, 6.0],

          'R': [3.0, 11.15],

          'N': [0.2, 5.41],

          ...

          'W': [-3.4, 5.89],

          'Y': [-2.3, 5.66],

          'V': [-1.5, 5.96]

        }

        where keys are amino acids and the list is hydrophobicity value and
        isoelectric point respectively.

        Returns
        -------
        dictionary
            A dictionary of amino acids and their associated HB and PI values.
        """

    @amino_acids.setter
    def amino_acids(self, value: typing.Union[str, pathlib.Path] = None) -> None:
        """
        Set the file path source for amino acid definitions before calling
        :meth:`_make_amino_acids` to build the dictionary representation.
        Additionally, a new pickle file is generating using the updated
        file definitions to reduce future computation time.

        Parameters
        ----------
        value
            str or Path-like representation of CSV file containing definitions
            for amino acids. This file is assumed to be of the form:

                AA,AA_abr,HB,PI

            where the amino acid, its abbreviation, hydrophobicity value and
            isoelectric point are present

        """

    def _make_amino_acids(self, ref_data: typing.Union[str, pathlib.Path] = None):
        """
        Construct a dictionary of amino acids values and
        their respective hydrophobicity values and isoelectric points.

        Parameters
        ----------
        ref_data
            str or Path-like representation of CSV file containing definitions
            for amino acids.
        """

    @staticmethod
    def is_low_allele_valid(allele: bool = True):
        """
        TODO: Currently unused imputation function preserved for future use.

        Parameters
        ----------
        allele
            allele

        Returns
        -------
        boolean
            Set to return True until future implementation.
        """

    @staticmethod
    def is_high_allele_valid(allele: bool = True):
        """
        TODO: Currently unused imputation function preserved for future use.

        Parameters
        ----------
        allele
            allele

        Returns
        -------
        boolean
            Set to return True until future implementation.
        """

    @staticmethod
    def split_broad_antigen(allele_str: str) -> typing.Tuple[str, str]:
        """
        Helper method for separating alleles from columns where multiple alleles
        are represented similar to the form:

            A24(9)

        as can occur in nomenclature for broad antigens.

        Parameters
        ----------
        allele_str
            string representation of broad antigen nomenclature similar to
            A23(9)

        Returns
        -------
        tuple
            Separated, formatted alleles similar to A23, A9
        """

    @staticmethod
    def translate_race(srtr_race: str) -> typing.Union[str, None]:
        """
        Helper method for translating race from SRTR description to shortened
        form for haplotype frequency lookups. Similar to:

            "Black or African American": "AFA",

            ...

            "White": "CAU",


        Parameters
        ----------
        srtr_race
            string representation of SRTR race description.
        Returns
        -------
        string
            Representation of haplotype frequency race.
        """

    @staticmethod
    def haplotype_frequency(
        allele_list: typing.List[str],
        ref_data: typing.Union[str, pathlib.Path],
    ) -> typing.Dict:
        """
        Construct an ordered hierarchical dictionary which maps A~B~DR~DQ
        haplotype to frequency.

        Parameters
        ----------
        allele_list
            list of ordered string representations for the target alleles to
            read from reference data. Similar to the form:

            ["A","B","DRB1"]

        ref_data
             str or Path-like representation of CSV file.
        Returns
        -------
        dictionary
            A hierarchical dictionary of allele combinations mapped to their
            overall frequency of appearance.
        """
        if ref_data is str:
            ref_data = pathlib.Path(ref_data)
        test = pandas.read_csv(ref_data)
        df = test.groupby(allele_list).agg({"frequency": "sum"}).reset_index()

        """
        Iterative solution to generating hierarchical dictionary that ultimately
        builds a nested dictionary similar in form to:

         'A*30:07': {'B*18:01': {
                                    'DRB1*03:01': 3.2e-08,
                                    'DRB1*11:01': 3.19e-08
                                },
                     'B*51:01': {
                                    'DRB1*03:01': 3.2e-08,
                                    'DRB1*11:01': 3.19e-08
                                },
                     'B*58:01': {
                                    'DRB1*15:03': 1.2789e-07
                                },
                     'B*81:01': {
                                    'DRB1*03:02': 1.278e-07
                                }
                    },
        """

    @property
    def haplotype_frequencies(self) -> typing.Dict:
        """
        Returns a data lookup dictionary for racial haplotype frequencies.

        Returns a dictionary similar to the following: {

        'CAU' : {

            'A*30:07': {

                'B*18:01': {

                    'DRB1*03:01': 3.2e-08,

                    'DRB1*11:01': 3.19e-08

                    },

                'B*51:01': {

                    'DRB1*03:01': 3.2e-08,

                    'DRB1*11:01': 3.19e-08

                    },

                'B*58:01': {

                    'DRB1*15:03': 1.2789e-07

                    },

                'B*81:01': {

                    'DRB1*03:02': 1.278e-07

                }

            },

        ...,

        }

        where keys are race categories and hierarchical dictionary maps
        A~B~DR~DQ haplotype to frequency.

        Returns
        -------
        dictionary
            A hierarchical dictionary of racial categories with associated
            allele combinations mapped to their overall frequency of appearance.

        """

    @haplotype_frequencies.setter
    def haplotype_frequencies(self, value: typing.Union[str, pathlib.Path]):
        """
        Set the directory path source for haplotype frequency definitions before
        calling :meth:`_make_haplotype_frequencies` to build the dictionary
        representation. Additionally, a new pickle file is generating using the
        updated file definitions to reduce future computation time.

        Parameters
        ----------
        value
            str or Path-like representation of directory containing CSV files
            with definitions for haplotype frequencies. Each file is assumed to
            be of the form:

                A,C,B,DRB3/4/5,DRB1,DQB1,frequency,count,

            where each row is an allele combination, frequency factor, and count.

        """

    def _make_haplotype_frequencies(
        self,
        ref_data: typing.Union[str, pathlib.Path] = pathlib.Path(
            "../data/haplotype_frequencies"
        ),
    ) -> None:
        """
        Construct a dictionary of racial haplotype frequencies.

        Parameters
        ----------
        ref_data
            str or Path-like representation of directory of CSV file/s or a CSV
            file containing definitions for amino acids.
        """

    @property
    def allele_mappings(self):
        """
        Returns a data lookup dictionary for alleles mapped to WHO assigned
        type. Similar to the following : {

            'A9': {'A23', 'A24'},

            'A10': {'A66', 'A25', 'A34', 'A26'},

            ...

            'DQ1': {'DQ6', 'DQ5'},

            'DQ3': {'DQ7', 'DQ8', 'DQ9'}

        }

        Returns
        -------
        dictionary
            A dictionary of HLA alleles containing parentheses(broad antigens)
            to WHO assigned type.
        """

    @allele_mappings.setter
    def allele_mappings(self, value):
        """
        Set the file path source for allele mappings before calling
        :meth:`_make_allele_mappings` to build the dictionary representation.
        Additionally, a new pickle file is generating using the updated
        file definitions to reduce future computation time.

        Parameters
        ----------
        value
            str or Path-like representation of CSV file containing definitions
            for amino acids. This file is assumed to be of the form:

                HLA_allele, Expert, Who

            where the Allele, expert assigned type, and WHO assigned type are
            present

        """

    def _make_allele_mappings(
        self,
        ref_data: typing.Union[str, pathlib.Path] = None,
    ) -> None:
        """
        Construct a dictionary of allele to WHO assigned type mappings.

        Parameters
        ----------
        ref_data
            str or Path-like representation of CSV file.
        """

    @property
    def allele_conversions(self):
        """
        Returns a data lookup dictionary for alleles mapped to expert assigned
        type. Similar to the following: {

            'A203': ['A*02:03'],

            'A210': ['A*02:10'],

            ...

            'DQ3': ['DQB1*03:06', 'DQB1*03:10', 'DQB1*03:14'],

            'DQ4': ['DQB1*04:01', 'DQB1*04:02']

        }

        Returns
        -------
        dictionary
            A dictionary of HLA alleles containing alleles to expert assigned
            type.

        """

    @allele_conversions.setter
    def allele_conversions(self, value: typing.Union[str, pathlib.Path] = None):
        """
        Set the file path source for allele conversions before calling
        :meth:`_make_allele_mappings` to build the dictionary representation.
        Additionally, a new pickle file is generating using the updated
        file definitions to reduce future computation time.

        Parameters
        ----------
        value
            str or Path-like representation of CSV file containing definitions
            for amino acids. This file is assumed to be of the form:

                HLA_allele, Expert, Who

            where the Allele, expert assigned type, and WHO assigned type are
            present

        """

    def _make_allele_conversions(
        self,
        ref_data: typing.Union[str, pathlib.Path] = None,
    ) -> None:
        """
        Construct a dictionary of allele to expert assigned type mappings.

        Parameters
        ----------
        ref_data
            str or Path-like representation of CSV file.
        """

    @staticmethod
    def impute_haplotype_pairs(two_haplotypes: typing.List[str]) -> typing.List[str]:
        """
        Replace the Null value HLA with the other one, if one of the two HLAs
        of same type does not have value

        Parameters
        ----------
        two_haplotypes
            pair of haplotypes to check for Null values.

        Returns
        -------
        list
            original haplotypes or imputed replacement haplotypes
        """

    def to_high_resolution(
        self, race: str, alleles: typing.List[float], allele_type_list: typing.List[str]
    ):
        """
        Convert supplied alleles into high resolution equivalents

        Parameters
        ----------
        race
            string representation of race
        alleles
            list of float SRTR allele values to convert to high resolution
        allele_type_list
            list of type of alleles supplied e.g. ['A','B','DR','DQ']

        Returns
        -------
        list
            high resolution alleles derived from haplotype frequency
            dictionaries.
        """

    # todo This function has a McCabe complexity of 12 and should be refactored
    def _low_resolution_to_high_resolution(
        self, race: str, alleles: typing.List, allele_type_list: typing.List[str]
    ):
        """
        Convert supplied alleles into high resolution equivalents

        Parameters
        ----------
        race
            string representation of race
        alleles
            list of float SRTR allele values to convert to high resolution
        allele_type_list
            list of type of alleles supplied e.g. ['A','B','DR','DQ']

        Returns
        -------
        list
            high resolution alleles derived from haplotype frequency
            dictionaries.
        """

    def _max_high_resolution_equivalent(
        self, one_haplotype, current_haplotype, frequency_dict: typing.Dict
    ):
        """
        Select highest frequency of occurrence high resolution allele from
        list of high resolution alleles corresponding to low resolution input.

        Parameters
        ----------
        one_haplotype

        current_haplotype

        frequency_dict
            dictionary of mapped allele values
        Returns
        -------

        """

    @staticmethod
    def _high_resolution_equivalents(
        low_resolution_haplotype: str,
        mapping_dict: typing.Dict[str, str],
        hla_dict: typing.Dict[str, str],
    ):
        """
        Get a list of high resolution haplotypes equivalents based on
        low resolution input.

        Parameters
        ----------
        low_resolution_haplotype
            string representation of low resolution haplotype
        mapping_dict
            dictionary of mapped allele values based on WHO assignments
        hla_dict
            dictionary of mapped allele values based on expert assignments
        Returns
        -------
        list
            high resolution alleles corresponding to low resolution input
        """

    @staticmethod
    def impute_haplotype(
        haplotype: str, allele_type_list: typing.List[str]
    ) -> typing.List[str]:
        """
        Replace Null value haplotype with string imputation.

        Parameters
        ----------
        haplotype
            string representation of haplotype being assessed.
        allele_type_list
            list of haplotype representations

        Returns
        -------
        list
            Unmodified or imputed halpotype.
        """

    def hlaii_predictor(
        self, donor_alleles: typing.List[str], recipient_alleles: typing.List[str]
    ) -> typing.Dict[str, float]:
        """
        Calculates the total electrostatic, hydrophobicity, and amino acid
        mismatch scores between a set of Donor/Recipient alleles.

        Parameters
        ----------
        donor_alleles
            List of Donor Alleles
        recipient_alleles
            List of Recipient Alleles
        Returns
        -------
        dict
            dictionary of electrostatic, hydrophobicity, and amino acid mismatch
            scores.
        """

    @staticmethod
    def mean_or_zero(values: typing.List[float]) -> typing.Union[float, int]:
        """
        Get the mean score of supplied amino-acid, hydrophobicity, or
        electrostatic mismatch scores. If the no scores are supplied, then the
        function instead returns zero.

        Parameters
        ----------
        values
            List of mismatch scores

        Returns
        -------
        float
            mean value of mismatch scores or zero in cases of no scores.
        """

    def hlai_predictor(
        self, donor_alleles: typing.List[str], recipient_alleles: typing.List[str]
    ) -> typing.Dict[str, float]:
        """
        Calculates the total electrostatic, hydrophobicity, and amino acid
        mismatch scores between a set of Donor/Recipient alleles.

        Parameters
        ----------
        donor_alleles
            List of Donor Alleles
        recipient_alleles
            List of Recipient Alleles
        Returns
        -------
        dict
            dictionary of electrostatic, hydrophobicity, and amino acid mismatch
            scores.
        """

    @functools.cache
    def _hlai_predictor(
        self, donor_allele: str, recipient_alleles: typing.List[str]
    ) -> typing.Dict[str, float]:
        """
        Calculates the total electrostatic, hydrophobicity, and amino acid
        mismatch scores between a Donor allele and a set of Recipient alleles.

        Parameters
        ----------
        donor_allele
            List of Donor Alleles
        recipient_alleles
            List of Recipient Alleles
        Returns
        -------
        dict
            dictionary of electrostatic, hydrophobicity, and amino acid mismatch
            scores.
        """
