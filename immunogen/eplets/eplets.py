import configparser
import inspect
import pathlib
import pickle
import typing
from collections import defaultdict

import pandas

try:
    import importlib.resources as pkg_resources
except ImportError:
    import importlib_resources as pkg_resources


class Eplets:
    """
    Class which maintains internal dictionaries of Allele to Eplet mappings.
    Includes precomputed mappings while permitting skilled users to supply different mappings.
    """

    @property
    def config(self):
        """
        Property reads configuration file for file paths to Allele/Eplet raw data.
        """

    @config.setter
    def config(
        self, value: typing.Optional[typing.Union[str, pathlib.Path]]
    ) -> configparser.ConfigParser():
        """

        :param value:
        :return:
        """


    def _make_config(self, value=None):
        """

        :param value:
        :return:
        """

    @property
    def eplets(self) -> typing.Dict[str, str]:
        """
        Property contains Allele to Eplet mappings.

        Returns
        -------
        dictionary
            A dictionary of allele values with mapped eplets.
        """

    @eplets.setter
    def eplets(self, value: typing.Union[str, pathlib.Path]) -> None:
        """

        :param value:
        :return:
        """

    def _make_eplets(self, ref_data: typing.Union[str, pathlib.Path]) -> None:
        """

        :param ref_data:
        :return:
        """

    @property
    def eplet_types(self) -> typing.Dict[str, str]:
        """
        Property contains eplet to eplet type mappings.

        Returns
        -------
        dictionary
            A dictionary of eplet values with mapped eplet types.
        """

    @eplet_types.setter
    def eplet_types(self, value: typing.Union[str, pathlib.Path]) -> None:
        """

        :param value:
        :return:
        """
    def _make_eplet_types(self, ref_data: typing.Union[str, pathlib.Path]) -> None:
        """

        :param ref_data:
        :return:
        """
    def compare(self, left: typing.List[str], right: typing.List[str]):
        """
        Returns a tuple of the list of eplets associated with the left alleles and the list of eplets associated with the right alleles.

        Parameters
        ----------
        left
            list of alleles
        right
            list of alleles
        Returns
        -------
        tuple
            A tuple of eplets associated with the unique alleles of both the left list of alleles and right list of alleles.
        """

    def get_eplet_type(self, left: typing.List[str], right: typing.List[str]):
        """
        Returns the overall mismatch scores between the left and right sets of eplets.
        The mismatch scores are divided into Oth and Abv type mismatches.

        Parameters
        ----------
        left
            list of eplets
        right
            list of eplets

        Returns
        -------
            tuple
                Abv mismatch total, Oth mismatch total, list of Abv mismatched eplets, list of Oth mismatched eplets
        """
