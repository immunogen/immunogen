immunogenicity package
----------------------------------------------

.. toctree::

    sequencetype
    immunogenicityutils

.. automodule:: immunogen.immunogenicity
   :members:
   :undoc-members:
   :show-inheritance:
